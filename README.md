# CWA Deanonymization

This project was created with [Angular CLI](https://github.com/angular/angular-cli) version 14.0.4.

## Installation
- Clone the repo
- Install [NodeJS](https://nodejs.org/) on your machine
- Run `npm install` in the project directory

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

You may need to run your browser with disabled web security, because of CORS problems. For Chrome you need to run it with this command: `path/to/chrome[.exe] --disable-web-security --user-data-dir="~/ChromeDev"`

Additionally, it may be necessary to install the local SSL certificate of the backend in your browser. It is located in the `cwa-server` repository under `docker-compose-test-secrets/ssl.p12` and uses the password `123456`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Useful commands

- To generate TypeScript Protobuf files: `protoc --plugin=protoc-gen-ts_proto=.\node_modules\.bin\protoc-gen-ts_proto.cmd --ts_proto_opt=useOptionals=all --ts_proto_out=. ./*.proto`
- To transfer infections to S3: `docker restart cwa-server_distribution_1 && docker wait cwa-server_distribution_1`
