import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { Buffer } from 'buffer';
import { DateTime } from 'luxon';
import { ModalComponent } from './components/modal.component';
import { CheckInService } from './services/check-in.service';
import { DownloadService } from './services/download.service';
import { SubmissionService } from './services/submission.service';
import { Utils } from './utils/utils';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  @ViewChild('dynamicContent', { read: ViewContainerRef, static: true })
  dynamicContent!: ViewContainerRef

  // https://e.coronawarn.app?v=1#CAESDwgBEgVNYXJrdBoEVGVzdBp2CAESYIMCzMxNed7UMADn9jGaFF1381k3ZUqX0vfYmX35nJsThvZ40iuEU4phThcA4erdE8sPF0dNboA2I7bisI47iuPTofNxVnLel6Xnz4vUpI8b78-d4vYGjJlBHPeqW7aGgBoQFtFE-09LWbCfMgcJSGkJnyIGCAEQAxh4

  // https://e.coronawarn.app?v=1#CAESJggBEg9QcmFrdGlrdW0gTVAyUFMaBVAtODAxKPSHjJYGMIyyjJYGGnYIARJggwLMzE153tQwAOf2MZoUXXfzWTdlSpfS99iZffmcmxOG9njSK4RTimFOFwDh6t0Tyw8XR01ugDYjtuKwjjuK49Oh83FWct6XpefPi9Skjxvvz53i9gaMmUEc96pbtoaAGhArdVu_bpNwiTTX_bJc5HC7IgYIARAJGFo

  newCode: string = '';

  constructor(
    public checkInService: CheckInService,
    public submissionService: SubmissionService,
    public downloadService: DownloadService
  ) { }

  async ngOnInit() {
    let codes = localStorage.getItem('qr-codes');

    if (codes !== null) {
      this.checkInService.qrCodes = JSON.parse(codes);
      this.refreshDecodedQrCodes();
    }

    this.downloadService.loadInfections();

    // setTimeout(() => {
    //   this.refreshDecodedQrCodes()
    // }, 100)
  }

  refreshDecodedQrCodes() {
    this.checkInService.locations = [];

    for (let i = 0; i < this.checkInService.qrCodes.length; i++) {
      const url = this.checkInService.qrCodes[i];
      const data = Utils.readQrCodeData(url);

      if (data) {
        this.checkInService.locations.push({
          index: i,
          locationId: Buffer.from(data.locationId).toString('base64'),
          description: data.message.locationData?.description!,
          address: data.message.locationData?.address!,
          eventStart: data.message.locationData?.startTimestamp,
          eventEnd: data.message.locationData?.endTimestamp,
        });
      }
    }
  }

  addCode() {
    let match = this.newCode.match(/https:\/\/e.coronawarn.app\?v=1#\S+/)

    if (match && this.checkInService.qrCodes.indexOf(match[0]) === -1) {
      this.checkInService.qrCodes.push(match[0]);
      this.newCode = '';
      this.refreshDecodedQrCodes();
      localStorage.setItem('qr-codes', JSON.stringify(this.checkInService.qrCodes))
    }
  }

  removeCode(index: number) {
    const modal = this.dynamicContent.createComponent(ModalComponent);

    modal.instance.title = 'Sind Sie sicher?';
    modal.instance.content = 'Möchten Sie diese Location wirklich löschen?';
    modal.instance.actionButtonText = 'Löschen';
    modal.instance.actionButton = () => {
      this.checkInService.qrCodes.splice(index, 1);
      this.refreshDecodedQrCodes();
      localStorage.setItem('qr-codes', JSON.stringify(this.checkInService.qrCodes));
      modal.destroy();
    }
    modal.instance.cancelButton = () => {
      modal.destroy();
    }
  }

  formatDate(date?: number) {
    if (date) {
      return DateTime.fromSeconds(date).toLocaleString({
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit'
      });
    }

    return '';
  }
}
