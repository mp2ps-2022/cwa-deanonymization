import { Component, Input } from "@angular/core";

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html'
})
export class ModalComponent {

  @Input('title')
  title!: string;

  @Input('content')
  content!: string;

  @Input('actionButtonText')
  actionButtonText!: string;

  actionButton!: () => void

  cancelButton!: () => void
}
