import { Pipe, PipeTransform } from '@angular/core';
import { Buffer } from 'buffer';

@Pipe({
  name: 'base64'
})
export class Base64Pipe implements PipeTransform {

  transform(value?: Uint8Array): string {
    if (value) {
      return Buffer.from(value).toString('base64');
    }

    return '';
  }
}
