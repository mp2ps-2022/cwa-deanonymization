/* eslint-disable */
import * as _m0 from "protobufjs/minimal";

export const protobufPackage = "";

export interface CheckIn {
  locationId?: Uint8Array;
  startIntervalNumber?: number;
  endIntervalNumber?: number;
  transmissionRiskLevel?: number;
}

export interface CheckInRecord {
  startIntervalNumber?: number;
  period?: number;
  transmissionRiskLevel?: number;
}

export interface CheckInProtectedReport {
  locationIdHash?: Uint8Array;
  iv?: Uint8Array;
  encryptedCheckInRecord?: Uint8Array;
  mac?: Uint8Array;
}

function createBaseCheckIn(): CheckIn {
  return {
    locationId: new Uint8Array(),
    startIntervalNumber: 0,
    endIntervalNumber: 0,
    transmissionRiskLevel: 0,
  };
}

export const CheckIn = {
  encode(
    message: CheckIn,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.locationId !== undefined && message.locationId.length !== 0) {
      writer.uint32(10).bytes(message.locationId);
    }
    if (
      message.startIntervalNumber !== undefined &&
      message.startIntervalNumber !== 0
    ) {
      writer.uint32(16).uint32(message.startIntervalNumber);
    }
    if (
      message.endIntervalNumber !== undefined &&
      message.endIntervalNumber !== 0
    ) {
      writer.uint32(24).uint32(message.endIntervalNumber);
    }
    if (
      message.transmissionRiskLevel !== undefined &&
      message.transmissionRiskLevel !== 0
    ) {
      writer.uint32(32).uint32(message.transmissionRiskLevel);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): CheckIn {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCheckIn();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.locationId = reader.bytes();
          break;
        case 2:
          message.startIntervalNumber = reader.uint32();
          break;
        case 3:
          message.endIntervalNumber = reader.uint32();
          break;
        case 4:
          message.transmissionRiskLevel = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CheckIn {
    return {
      locationId: isSet(object.locationId)
        ? bytesFromBase64(object.locationId)
        : new Uint8Array(),
      startIntervalNumber: isSet(object.startIntervalNumber)
        ? Number(object.startIntervalNumber)
        : 0,
      endIntervalNumber: isSet(object.endIntervalNumber)
        ? Number(object.endIntervalNumber)
        : 0,
      transmissionRiskLevel: isSet(object.transmissionRiskLevel)
        ? Number(object.transmissionRiskLevel)
        : 0,
    };
  },

  toJSON(message: CheckIn): unknown {
    const obj: any = {};
    message.locationId !== undefined &&
      (obj.locationId = base64FromBytes(
        message.locationId !== undefined ? message.locationId : new Uint8Array()
      ));
    message.startIntervalNumber !== undefined &&
      (obj.startIntervalNumber = Math.round(message.startIntervalNumber));
    message.endIntervalNumber !== undefined &&
      (obj.endIntervalNumber = Math.round(message.endIntervalNumber));
    message.transmissionRiskLevel !== undefined &&
      (obj.transmissionRiskLevel = Math.round(message.transmissionRiskLevel));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<CheckIn>, I>>(object: I): CheckIn {
    const message = createBaseCheckIn();
    message.locationId = object.locationId ?? new Uint8Array();
    message.startIntervalNumber = object.startIntervalNumber ?? 0;
    message.endIntervalNumber = object.endIntervalNumber ?? 0;
    message.transmissionRiskLevel = object.transmissionRiskLevel ?? 0;
    return message;
  },
};

function createBaseCheckInRecord(): CheckInRecord {
  return { startIntervalNumber: 0, period: 0, transmissionRiskLevel: 0 };
}

export const CheckInRecord = {
  encode(
    message: CheckInRecord,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (
      message.startIntervalNumber !== undefined &&
      message.startIntervalNumber !== 0
    ) {
      writer.uint32(8).uint32(message.startIntervalNumber);
    }
    if (message.period !== undefined && message.period !== 0) {
      writer.uint32(16).uint32(message.period);
    }
    if (
      message.transmissionRiskLevel !== undefined &&
      message.transmissionRiskLevel !== 0
    ) {
      writer.uint32(24).uint32(message.transmissionRiskLevel);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): CheckInRecord {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCheckInRecord();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.startIntervalNumber = reader.uint32();
          break;
        case 2:
          message.period = reader.uint32();
          break;
        case 3:
          message.transmissionRiskLevel = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CheckInRecord {
    return {
      startIntervalNumber: isSet(object.startIntervalNumber)
        ? Number(object.startIntervalNumber)
        : 0,
      period: isSet(object.period) ? Number(object.period) : 0,
      transmissionRiskLevel: isSet(object.transmissionRiskLevel)
        ? Number(object.transmissionRiskLevel)
        : 0,
    };
  },

  toJSON(message: CheckInRecord): unknown {
    const obj: any = {};
    message.startIntervalNumber !== undefined &&
      (obj.startIntervalNumber = Math.round(message.startIntervalNumber));
    message.period !== undefined && (obj.period = Math.round(message.period));
    message.transmissionRiskLevel !== undefined &&
      (obj.transmissionRiskLevel = Math.round(message.transmissionRiskLevel));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<CheckInRecord>, I>>(
    object: I
  ): CheckInRecord {
    const message = createBaseCheckInRecord();
    message.startIntervalNumber = object.startIntervalNumber ?? 0;
    message.period = object.period ?? 0;
    message.transmissionRiskLevel = object.transmissionRiskLevel ?? 0;
    return message;
  },
};

function createBaseCheckInProtectedReport(): CheckInProtectedReport {
  return {
    locationIdHash: new Uint8Array(),
    iv: new Uint8Array(),
    encryptedCheckInRecord: new Uint8Array(),
    mac: new Uint8Array(),
  };
}

export const CheckInProtectedReport = {
  encode(
    message: CheckInProtectedReport,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (
      message.locationIdHash !== undefined &&
      message.locationIdHash.length !== 0
    ) {
      writer.uint32(10).bytes(message.locationIdHash);
    }
    if (message.iv !== undefined && message.iv.length !== 0) {
      writer.uint32(18).bytes(message.iv);
    }
    if (
      message.encryptedCheckInRecord !== undefined &&
      message.encryptedCheckInRecord.length !== 0
    ) {
      writer.uint32(26).bytes(message.encryptedCheckInRecord);
    }
    if (message.mac !== undefined && message.mac.length !== 0) {
      writer.uint32(34).bytes(message.mac);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CheckInProtectedReport {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCheckInProtectedReport();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.locationIdHash = reader.bytes();
          break;
        case 2:
          message.iv = reader.bytes();
          break;
        case 3:
          message.encryptedCheckInRecord = reader.bytes();
          break;
        case 4:
          message.mac = reader.bytes();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CheckInProtectedReport {
    return {
      locationIdHash: isSet(object.locationIdHash)
        ? bytesFromBase64(object.locationIdHash)
        : new Uint8Array(),
      iv: isSet(object.iv) ? bytesFromBase64(object.iv) : new Uint8Array(),
      encryptedCheckInRecord: isSet(object.encryptedCheckInRecord)
        ? bytesFromBase64(object.encryptedCheckInRecord)
        : new Uint8Array(),
      mac: isSet(object.mac) ? bytesFromBase64(object.mac) : new Uint8Array(),
    };
  },

  toJSON(message: CheckInProtectedReport): unknown {
    const obj: any = {};
    message.locationIdHash !== undefined &&
      (obj.locationIdHash = base64FromBytes(
        message.locationIdHash !== undefined
          ? message.locationIdHash
          : new Uint8Array()
      ));
    message.iv !== undefined &&
      (obj.iv = base64FromBytes(
        message.iv !== undefined ? message.iv : new Uint8Array()
      ));
    message.encryptedCheckInRecord !== undefined &&
      (obj.encryptedCheckInRecord = base64FromBytes(
        message.encryptedCheckInRecord !== undefined
          ? message.encryptedCheckInRecord
          : new Uint8Array()
      ));
    message.mac !== undefined &&
      (obj.mac = base64FromBytes(
        message.mac !== undefined ? message.mac : new Uint8Array()
      ));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<CheckInProtectedReport>, I>>(
    object: I
  ): CheckInProtectedReport {
    const message = createBaseCheckInProtectedReport();
    message.locationIdHash = object.locationIdHash ?? new Uint8Array();
    message.iv = object.iv ?? new Uint8Array();
    message.encryptedCheckInRecord =
      object.encryptedCheckInRecord ?? new Uint8Array();
    message.mac = object.mac ?? new Uint8Array();
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

const atob: (b64: string) => string =
  globalThis.atob ||
  ((b64) => globalThis.Buffer.from(b64, "base64").toString("binary"));
function bytesFromBase64(b64: string): Uint8Array {
  const bin = atob(b64);
  const arr = new Uint8Array(bin.length);
  for (let i = 0; i < bin.length; ++i) {
    arr[i] = bin.charCodeAt(i);
  }
  return arr;
}

const btoa: (bin: string) => string =
  globalThis.btoa ||
  ((bin) => globalThis.Buffer.from(bin, "binary").toString("base64"));
function base64FromBytes(arr: Uint8Array): string {
  const bin: string[] = [];
  arr.forEach((byte) => {
    bin.push(String.fromCharCode(byte));
  });
  return btoa(bin.join(""));
}

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;

export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin
  ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & Record<
        Exclude<keyof I, KeysOfUnion<P>>,
        never
      >;

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
