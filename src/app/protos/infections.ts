/* eslint-disable */
import * as Long from "long";
import * as _m0 from "protobufjs/minimal";

export const protobufPackage = "";

/**
 * Protobuf definition for exports of confirmed temporary exposure keys.
 *
 * The full file format is documented under "Exposure Key Export File Format
 * and Verification" at https://www.google.com/covid19/exposurenotifications/
 *
 * These files have a 16-byte, space-padded header before the protobuf data
 * starts. They will be contained in a zip archive, alongside a signature
 * file verifying the contents.
 */
export interface TemporaryExposureKeyExport {
  /**
   * Time window of keys in this file based on arrival to server, in UTC
   * seconds. start_timestamp, end_timestamp, and batch_num must be unique
   * at any given snapshot of the index for a server. If multiple
   * files are used for a specific time period, and batch_num/batch_size
   * are both 1 (See below), then offsetting the end_timestamp is the
   * suggested method for forcing uniqueness.
   */
  startTimestamp?: number;
  endTimestamp?: number;
  /** Region for which these keys came from (e.g., country) */
  region?: string;
  /**
   * E.g., Batch 2 of 10. Ordinal, 1-based numbering.
   * Note: Not yet supported on iOS. Use values of 1 for both.
   */
  batchNum?: number;
  batchSize?: number;
  /**
   * Information about signatures
   * If there are multiple entries, they must be ordered in descending
   * time order by signing key effective time (most recent one first).
   * There is a limit of 10 signature infos per export file (mobile OS may
   * not check anything after that).
   */
  signatureInfos?: SignatureInfo[];
  /**
   * The TemporaryExposureKeys for initial release of keys.
   * Keys should be included in this list for initial release,
   * whereas revised or revoked keys should go in revised_keys.
   */
  keys?: TemporaryExposureKey[];
  /**
   * TemporaryExposureKeys that have changed status.
   * Keys should be included in this list if they have changed status
   * or have been revoked.
   */
  revisedKeys?: TemporaryExposureKey[];
}

export interface SignatureInfo {
  /**
   * Key version for rollovers
   * Must be in character class [a-zA-Z0-9_]. E.g., 'v1'
   */
  verificationKeyVersion?: string;
  /**
   * Alias with which to identify public key to be used for verification
   * Must be in character class [a-zA-Z0-9_]
   * For cross-compatibility with Apple, use MCC
   * (https://en.wikipedia.org/wiki/Mobile_country_code).
   */
  verificationKeyId?: string;
  /**
   * ASN.1 OID for Algorithm Identifier. Supported algorithms are
   * either 1.2.840.10045.4.3.2 or 1.2.840.10045.4.3.4
   */
  signatureAlgorithm?: string;
}

export interface TemporaryExposureKey {
  /** Key of infected user */
  keyData?: Uint8Array;
  /**
   * Varying risks associated with exposure depending on type of verification
   * Ignored by the v1.5 client API when report_type is set.
   *
   * @deprecated
   */
  transmissionRiskLevel?: number;
  /** The interval number since epoch for which a key starts */
  rollingStartIntervalNumber?: number;
  /** Increments of 10 minutes describing how long a key is valid */
  rollingPeriod?: number;
  /** Type of diagnosis associated with a key. */
  reportType?: TemporaryExposureKey_ReportType;
  /**
   * Number of days elapsed between symptom onset and the TEK being used.
   * E.g. 2 means TEK is 2 days after onset of symptoms.
   */
  daysSinceOnsetOfSymptoms?: number;
  /** Type of variant of concern associated with a key. */
  variantOfConcern?: TemporaryExposureKey_VariantOfConcern;
}

/** Data type representing why this key was published. */
export enum TemporaryExposureKey_ReportType {
  /** UNKNOWN - Never returned by the client API. */
  UNKNOWN = 0,
  CONFIRMED_TEST = 1,
  CONFIRMED_CLINICAL_DIAGNOSIS = 2,
  SELF_REPORT = 3,
  RECURSIVE = 4,
  /** REVOKED - Used to revoke a key, never returned by client API. */
  REVOKED = 5,
  UNRECOGNIZED = -1,
}

export function temporaryExposureKey_ReportTypeFromJSON(
  object: any
): TemporaryExposureKey_ReportType {
  switch (object) {
    case 0:
    case "UNKNOWN":
      return TemporaryExposureKey_ReportType.UNKNOWN;
    case 1:
    case "CONFIRMED_TEST":
      return TemporaryExposureKey_ReportType.CONFIRMED_TEST;
    case 2:
    case "CONFIRMED_CLINICAL_DIAGNOSIS":
      return TemporaryExposureKey_ReportType.CONFIRMED_CLINICAL_DIAGNOSIS;
    case 3:
    case "SELF_REPORT":
      return TemporaryExposureKey_ReportType.SELF_REPORT;
    case 4:
    case "RECURSIVE":
      return TemporaryExposureKey_ReportType.RECURSIVE;
    case 5:
    case "REVOKED":
      return TemporaryExposureKey_ReportType.REVOKED;
    case -1:
    case "UNRECOGNIZED":
    default:
      return TemporaryExposureKey_ReportType.UNRECOGNIZED;
  }
}

export function temporaryExposureKey_ReportTypeToJSON(
  object: TemporaryExposureKey_ReportType
): string {
  switch (object) {
    case TemporaryExposureKey_ReportType.UNKNOWN:
      return "UNKNOWN";
    case TemporaryExposureKey_ReportType.CONFIRMED_TEST:
      return "CONFIRMED_TEST";
    case TemporaryExposureKey_ReportType.CONFIRMED_CLINICAL_DIAGNOSIS:
      return "CONFIRMED_CLINICAL_DIAGNOSIS";
    case TemporaryExposureKey_ReportType.SELF_REPORT:
      return "SELF_REPORT";
    case TemporaryExposureKey_ReportType.RECURSIVE:
      return "RECURSIVE";
    case TemporaryExposureKey_ReportType.REVOKED:
      return "REVOKED";
    case TemporaryExposureKey_ReportType.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

/** Data type representing a variant of concern. */
export enum TemporaryExposureKey_VariantOfConcern {
  VARIANT_TYPE_UNKNOWN = 0,
  /** VARIANT_TYPE_1 - Vaccine is effective */
  VARIANT_TYPE_1 = 1,
  /** VARIANT_TYPE_2 - Highly transmissive */
  VARIANT_TYPE_2 = 2,
  /** VARIANT_TYPE_3 - High severity */
  VARIANT_TYPE_3 = 3,
  /** VARIANT_TYPE_4 - Vaccine breakthrough */
  VARIANT_TYPE_4 = 4,
  UNRECOGNIZED = -1,
}

export function temporaryExposureKey_VariantOfConcernFromJSON(
  object: any
): TemporaryExposureKey_VariantOfConcern {
  switch (object) {
    case 0:
    case "VARIANT_TYPE_UNKNOWN":
      return TemporaryExposureKey_VariantOfConcern.VARIANT_TYPE_UNKNOWN;
    case 1:
    case "VARIANT_TYPE_1":
      return TemporaryExposureKey_VariantOfConcern.VARIANT_TYPE_1;
    case 2:
    case "VARIANT_TYPE_2":
      return TemporaryExposureKey_VariantOfConcern.VARIANT_TYPE_2;
    case 3:
    case "VARIANT_TYPE_3":
      return TemporaryExposureKey_VariantOfConcern.VARIANT_TYPE_3;
    case 4:
    case "VARIANT_TYPE_4":
      return TemporaryExposureKey_VariantOfConcern.VARIANT_TYPE_4;
    case -1:
    case "UNRECOGNIZED":
    default:
      return TemporaryExposureKey_VariantOfConcern.UNRECOGNIZED;
  }
}

export function temporaryExposureKey_VariantOfConcernToJSON(
  object: TemporaryExposureKey_VariantOfConcern
): string {
  switch (object) {
    case TemporaryExposureKey_VariantOfConcern.VARIANT_TYPE_UNKNOWN:
      return "VARIANT_TYPE_UNKNOWN";
    case TemporaryExposureKey_VariantOfConcern.VARIANT_TYPE_1:
      return "VARIANT_TYPE_1";
    case TemporaryExposureKey_VariantOfConcern.VARIANT_TYPE_2:
      return "VARIANT_TYPE_2";
    case TemporaryExposureKey_VariantOfConcern.VARIANT_TYPE_3:
      return "VARIANT_TYPE_3";
    case TemporaryExposureKey_VariantOfConcern.VARIANT_TYPE_4:
      return "VARIANT_TYPE_4";
    case TemporaryExposureKey_VariantOfConcern.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface TEKSignatureList {
  /**
   * When there are multiple signatures, they must be sorted in time order
   * by first effective date for the signing key in descending order.
   * The most recent effective signing key must appear first.
   * There is a limit of 10 signature infos per export file (mobile OS may
   * not check anything after that).
   */
  signatures?: TEKSignature[];
}

export interface TEKSignature {
  /** Info about the signing key, version, algorithm, etc. */
  signatureInfo?: SignatureInfo;
  /**
   * E.g., Batch 2 of 10
   * Must match fields from TemporaryExposureKeyExport, see
   * documentation on that message.
   */
  batchNum?: number;
  batchSize?: number;
  /** Signature in X9.62 format (ASN.1 SEQUENCE of two INTEGER fields) */
  signature?: Uint8Array;
}

function createBaseTemporaryExposureKeyExport(): TemporaryExposureKeyExport {
  return {
    startTimestamp: 0,
    endTimestamp: 0,
    region: "",
    batchNum: 0,
    batchSize: 0,
    signatureInfos: [],
    keys: [],
    revisedKeys: [],
  };
}

export const TemporaryExposureKeyExport = {
  encode(
    message: TemporaryExposureKeyExport,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.startTimestamp !== undefined && message.startTimestamp !== 0) {
      writer.uint32(9).fixed64(message.startTimestamp);
    }
    if (message.endTimestamp !== undefined && message.endTimestamp !== 0) {
      writer.uint32(17).fixed64(message.endTimestamp);
    }
    if (message.region !== undefined && message.region !== "") {
      writer.uint32(26).string(message.region);
    }
    if (message.batchNum !== undefined && message.batchNum !== 0) {
      writer.uint32(32).int32(message.batchNum);
    }
    if (message.batchSize !== undefined && message.batchSize !== 0) {
      writer.uint32(40).int32(message.batchSize);
    }
    if (
      message.signatureInfos !== undefined &&
      message.signatureInfos.length !== 0
    ) {
      for (const v of message.signatureInfos) {
        SignatureInfo.encode(v!, writer.uint32(50).fork()).ldelim();
      }
    }
    if (message.keys !== undefined && message.keys.length !== 0) {
      for (const v of message.keys) {
        TemporaryExposureKey.encode(v!, writer.uint32(58).fork()).ldelim();
      }
    }
    if (message.revisedKeys !== undefined && message.revisedKeys.length !== 0) {
      for (const v of message.revisedKeys) {
        TemporaryExposureKey.encode(v!, writer.uint32(66).fork()).ldelim();
      }
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): TemporaryExposureKeyExport {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseTemporaryExposureKeyExport();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.startTimestamp = longToNumber(reader.fixed64() as Long);
          break;
        case 2:
          message.endTimestamp = longToNumber(reader.fixed64() as Long);
          break;
        case 3:
          message.region = reader.string();
          break;
        case 4:
          message.batchNum = reader.int32();
          break;
        case 5:
          message.batchSize = reader.int32();
          break;
        case 6:
          message.signatureInfos!.push(
            SignatureInfo.decode(reader, reader.uint32())
          );
          break;
        case 7:
          message.keys!.push(
            TemporaryExposureKey.decode(reader, reader.uint32())
          );
          break;
        case 8:
          message.revisedKeys!.push(
            TemporaryExposureKey.decode(reader, reader.uint32())
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): TemporaryExposureKeyExport {
    return {
      startTimestamp: isSet(object.startTimestamp)
        ? Number(object.startTimestamp)
        : 0,
      endTimestamp: isSet(object.endTimestamp)
        ? Number(object.endTimestamp)
        : 0,
      region: isSet(object.region) ? String(object.region) : "",
      batchNum: isSet(object.batchNum) ? Number(object.batchNum) : 0,
      batchSize: isSet(object.batchSize) ? Number(object.batchSize) : 0,
      signatureInfos: Array.isArray(object?.signatureInfos)
        ? object.signatureInfos.map((e: any) => SignatureInfo.fromJSON(e))
        : [],
      keys: Array.isArray(object?.keys)
        ? object.keys.map((e: any) => TemporaryExposureKey.fromJSON(e))
        : [],
      revisedKeys: Array.isArray(object?.revisedKeys)
        ? object.revisedKeys.map((e: any) => TemporaryExposureKey.fromJSON(e))
        : [],
    };
  },

  toJSON(message: TemporaryExposureKeyExport): unknown {
    const obj: any = {};
    message.startTimestamp !== undefined &&
      (obj.startTimestamp = Math.round(message.startTimestamp));
    message.endTimestamp !== undefined &&
      (obj.endTimestamp = Math.round(message.endTimestamp));
    message.region !== undefined && (obj.region = message.region);
    message.batchNum !== undefined &&
      (obj.batchNum = Math.round(message.batchNum));
    message.batchSize !== undefined &&
      (obj.batchSize = Math.round(message.batchSize));
    if (message.signatureInfos) {
      obj.signatureInfos = message.signatureInfos.map((e) =>
        e ? SignatureInfo.toJSON(e) : undefined
      );
    } else {
      obj.signatureInfos = [];
    }
    if (message.keys) {
      obj.keys = message.keys.map((e) =>
        e ? TemporaryExposureKey.toJSON(e) : undefined
      );
    } else {
      obj.keys = [];
    }
    if (message.revisedKeys) {
      obj.revisedKeys = message.revisedKeys.map((e) =>
        e ? TemporaryExposureKey.toJSON(e) : undefined
      );
    } else {
      obj.revisedKeys = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<TemporaryExposureKeyExport>, I>>(
    object: I
  ): TemporaryExposureKeyExport {
    const message = createBaseTemporaryExposureKeyExport();
    message.startTimestamp = object.startTimestamp ?? 0;
    message.endTimestamp = object.endTimestamp ?? 0;
    message.region = object.region ?? "";
    message.batchNum = object.batchNum ?? 0;
    message.batchSize = object.batchSize ?? 0;
    message.signatureInfos =
      object.signatureInfos?.map((e) => SignatureInfo.fromPartial(e)) || [];
    message.keys =
      object.keys?.map((e) => TemporaryExposureKey.fromPartial(e)) || [];
    message.revisedKeys =
      object.revisedKeys?.map((e) => TemporaryExposureKey.fromPartial(e)) || [];
    return message;
  },
};

function createBaseSignatureInfo(): SignatureInfo {
  return {
    verificationKeyVersion: "",
    verificationKeyId: "",
    signatureAlgorithm: "",
  };
}

export const SignatureInfo = {
  encode(
    message: SignatureInfo,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (
      message.verificationKeyVersion !== undefined &&
      message.verificationKeyVersion !== ""
    ) {
      writer.uint32(26).string(message.verificationKeyVersion);
    }
    if (
      message.verificationKeyId !== undefined &&
      message.verificationKeyId !== ""
    ) {
      writer.uint32(34).string(message.verificationKeyId);
    }
    if (
      message.signatureAlgorithm !== undefined &&
      message.signatureAlgorithm !== ""
    ) {
      writer.uint32(42).string(message.signatureAlgorithm);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SignatureInfo {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseSignatureInfo();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 3:
          message.verificationKeyVersion = reader.string();
          break;
        case 4:
          message.verificationKeyId = reader.string();
          break;
        case 5:
          message.signatureAlgorithm = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): SignatureInfo {
    return {
      verificationKeyVersion: isSet(object.verificationKeyVersion)
        ? String(object.verificationKeyVersion)
        : "",
      verificationKeyId: isSet(object.verificationKeyId)
        ? String(object.verificationKeyId)
        : "",
      signatureAlgorithm: isSet(object.signatureAlgorithm)
        ? String(object.signatureAlgorithm)
        : "",
    };
  },

  toJSON(message: SignatureInfo): unknown {
    const obj: any = {};
    message.verificationKeyVersion !== undefined &&
      (obj.verificationKeyVersion = message.verificationKeyVersion);
    message.verificationKeyId !== undefined &&
      (obj.verificationKeyId = message.verificationKeyId);
    message.signatureAlgorithm !== undefined &&
      (obj.signatureAlgorithm = message.signatureAlgorithm);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<SignatureInfo>, I>>(
    object: I
  ): SignatureInfo {
    const message = createBaseSignatureInfo();
    message.verificationKeyVersion = object.verificationKeyVersion ?? "";
    message.verificationKeyId = object.verificationKeyId ?? "";
    message.signatureAlgorithm = object.signatureAlgorithm ?? "";
    return message;
  },
};

function createBaseTemporaryExposureKey(): TemporaryExposureKey {
  return {
    keyData: new Uint8Array(),
    transmissionRiskLevel: 0,
    rollingStartIntervalNumber: 0,
    rollingPeriod: 0,
    reportType: 0,
    daysSinceOnsetOfSymptoms: 0,
    variantOfConcern: 0,
  };
}

export const TemporaryExposureKey = {
  encode(
    message: TemporaryExposureKey,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.keyData !== undefined && message.keyData.length !== 0) {
      writer.uint32(10).bytes(message.keyData);
    }
    if (
      message.transmissionRiskLevel !== undefined &&
      message.transmissionRiskLevel !== 0
    ) {
      writer.uint32(16).int32(message.transmissionRiskLevel);
    }
    if (
      message.rollingStartIntervalNumber !== undefined &&
      message.rollingStartIntervalNumber !== 0
    ) {
      writer.uint32(24).int32(message.rollingStartIntervalNumber);
    }
    if (message.rollingPeriod !== undefined && message.rollingPeriod !== 0) {
      writer.uint32(32).int32(message.rollingPeriod);
    }
    if (message.reportType !== undefined && message.reportType !== 0) {
      writer.uint32(40).int32(message.reportType);
    }
    if (
      message.daysSinceOnsetOfSymptoms !== undefined &&
      message.daysSinceOnsetOfSymptoms !== 0
    ) {
      writer.uint32(48).sint32(message.daysSinceOnsetOfSymptoms);
    }
    if (
      message.variantOfConcern !== undefined &&
      message.variantOfConcern !== 0
    ) {
      writer.uint32(64).int32(message.variantOfConcern);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): TemporaryExposureKey {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseTemporaryExposureKey();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.keyData = reader.bytes();
          break;
        case 2:
          message.transmissionRiskLevel = reader.int32();
          break;
        case 3:
          message.rollingStartIntervalNumber = reader.int32();
          break;
        case 4:
          message.rollingPeriod = reader.int32();
          break;
        case 5:
          message.reportType = reader.int32() as any;
          break;
        case 6:
          message.daysSinceOnsetOfSymptoms = reader.sint32();
          break;
        case 8:
          message.variantOfConcern = reader.int32() as any;
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): TemporaryExposureKey {
    return {
      keyData: isSet(object.keyData)
        ? bytesFromBase64(object.keyData)
        : new Uint8Array(),
      transmissionRiskLevel: isSet(object.transmissionRiskLevel)
        ? Number(object.transmissionRiskLevel)
        : 0,
      rollingStartIntervalNumber: isSet(object.rollingStartIntervalNumber)
        ? Number(object.rollingStartIntervalNumber)
        : 0,
      rollingPeriod: isSet(object.rollingPeriod)
        ? Number(object.rollingPeriod)
        : 0,
      reportType: isSet(object.reportType)
        ? temporaryExposureKey_ReportTypeFromJSON(object.reportType)
        : 0,
      daysSinceOnsetOfSymptoms: isSet(object.daysSinceOnsetOfSymptoms)
        ? Number(object.daysSinceOnsetOfSymptoms)
        : 0,
      variantOfConcern: isSet(object.variantOfConcern)
        ? temporaryExposureKey_VariantOfConcernFromJSON(object.variantOfConcern)
        : 0,
    };
  },

  toJSON(message: TemporaryExposureKey): unknown {
    const obj: any = {};
    message.keyData !== undefined &&
      (obj.keyData = base64FromBytes(
        message.keyData !== undefined ? message.keyData : new Uint8Array()
      ));
    message.transmissionRiskLevel !== undefined &&
      (obj.transmissionRiskLevel = Math.round(message.transmissionRiskLevel));
    message.rollingStartIntervalNumber !== undefined &&
      (obj.rollingStartIntervalNumber = Math.round(
        message.rollingStartIntervalNumber
      ));
    message.rollingPeriod !== undefined &&
      (obj.rollingPeriod = Math.round(message.rollingPeriod));
    message.reportType !== undefined &&
      (obj.reportType = temporaryExposureKey_ReportTypeToJSON(
        message.reportType
      ));
    message.daysSinceOnsetOfSymptoms !== undefined &&
      (obj.daysSinceOnsetOfSymptoms = Math.round(
        message.daysSinceOnsetOfSymptoms
      ));
    message.variantOfConcern !== undefined &&
      (obj.variantOfConcern = temporaryExposureKey_VariantOfConcernToJSON(
        message.variantOfConcern
      ));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<TemporaryExposureKey>, I>>(
    object: I
  ): TemporaryExposureKey {
    const message = createBaseTemporaryExposureKey();
    message.keyData = object.keyData ?? new Uint8Array();
    message.transmissionRiskLevel = object.transmissionRiskLevel ?? 0;
    message.rollingStartIntervalNumber = object.rollingStartIntervalNumber ?? 0;
    message.rollingPeriod = object.rollingPeriod ?? 0;
    message.reportType = object.reportType ?? 0;
    message.daysSinceOnsetOfSymptoms = object.daysSinceOnsetOfSymptoms ?? 0;
    message.variantOfConcern = object.variantOfConcern ?? 0;
    return message;
  },
};

function createBaseTEKSignatureList(): TEKSignatureList {
  return { signatures: [] };
}

export const TEKSignatureList = {
  encode(
    message: TEKSignatureList,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.signatures !== undefined && message.signatures.length !== 0) {
      for (const v of message.signatures) {
        TEKSignature.encode(v!, writer.uint32(10).fork()).ldelim();
      }
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): TEKSignatureList {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseTEKSignatureList();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.signatures!.push(
            TEKSignature.decode(reader, reader.uint32())
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): TEKSignatureList {
    return {
      signatures: Array.isArray(object?.signatures)
        ? object.signatures.map((e: any) => TEKSignature.fromJSON(e))
        : [],
    };
  },

  toJSON(message: TEKSignatureList): unknown {
    const obj: any = {};
    if (message.signatures) {
      obj.signatures = message.signatures.map((e) =>
        e ? TEKSignature.toJSON(e) : undefined
      );
    } else {
      obj.signatures = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<TEKSignatureList>, I>>(
    object: I
  ): TEKSignatureList {
    const message = createBaseTEKSignatureList();
    message.signatures =
      object.signatures?.map((e) => TEKSignature.fromPartial(e)) || [];
    return message;
  },
};

function createBaseTEKSignature(): TEKSignature {
  return {
    signatureInfo: undefined,
    batchNum: 0,
    batchSize: 0,
    signature: new Uint8Array(),
  };
}

export const TEKSignature = {
  encode(
    message: TEKSignature,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.signatureInfo !== undefined) {
      SignatureInfo.encode(
        message.signatureInfo,
        writer.uint32(10).fork()
      ).ldelim();
    }
    if (message.batchNum !== undefined && message.batchNum !== 0) {
      writer.uint32(16).int32(message.batchNum);
    }
    if (message.batchSize !== undefined && message.batchSize !== 0) {
      writer.uint32(24).int32(message.batchSize);
    }
    if (message.signature !== undefined && message.signature.length !== 0) {
      writer.uint32(34).bytes(message.signature);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): TEKSignature {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseTEKSignature();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.signatureInfo = SignatureInfo.decode(reader, reader.uint32());
          break;
        case 2:
          message.batchNum = reader.int32();
          break;
        case 3:
          message.batchSize = reader.int32();
          break;
        case 4:
          message.signature = reader.bytes();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): TEKSignature {
    return {
      signatureInfo: isSet(object.signatureInfo)
        ? SignatureInfo.fromJSON(object.signatureInfo)
        : undefined,
      batchNum: isSet(object.batchNum) ? Number(object.batchNum) : 0,
      batchSize: isSet(object.batchSize) ? Number(object.batchSize) : 0,
      signature: isSet(object.signature)
        ? bytesFromBase64(object.signature)
        : new Uint8Array(),
    };
  },

  toJSON(message: TEKSignature): unknown {
    const obj: any = {};
    message.signatureInfo !== undefined &&
      (obj.signatureInfo = message.signatureInfo
        ? SignatureInfo.toJSON(message.signatureInfo)
        : undefined);
    message.batchNum !== undefined &&
      (obj.batchNum = Math.round(message.batchNum));
    message.batchSize !== undefined &&
      (obj.batchSize = Math.round(message.batchSize));
    message.signature !== undefined &&
      (obj.signature = base64FromBytes(
        message.signature !== undefined ? message.signature : new Uint8Array()
      ));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<TEKSignature>, I>>(
    object: I
  ): TEKSignature {
    const message = createBaseTEKSignature();
    message.signatureInfo =
      object.signatureInfo !== undefined && object.signatureInfo !== null
        ? SignatureInfo.fromPartial(object.signatureInfo)
        : undefined;
    message.batchNum = object.batchNum ?? 0;
    message.batchSize = object.batchSize ?? 0;
    message.signature = object.signature ?? new Uint8Array();
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

const atob: (b64: string) => string =
  globalThis.atob ||
  ((b64) => globalThis.Buffer.from(b64, "base64").toString("binary"));
function bytesFromBase64(b64: string): Uint8Array {
  const bin = atob(b64);
  const arr = new Uint8Array(bin.length);
  for (let i = 0; i < bin.length; ++i) {
    arr[i] = bin.charCodeAt(i);
  }
  return arr;
}

const btoa: (bin: string) => string =
  globalThis.btoa ||
  ((bin) => globalThis.Buffer.from(bin, "binary").toString("base64"));
function base64FromBytes(arr: Uint8Array): string {
  const bin: string[] = [];
  arr.forEach((byte) => {
    bin.push(String.fromCharCode(byte));
  });
  return btoa(bin.join(""));
}

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;

export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin
  ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & Record<
        Exclude<keyof I, KeysOfUnion<P>>,
        never
      >;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
