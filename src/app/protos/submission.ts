/* eslint-disable */
import { TemporaryExposureKey } from "./infections";
import { CheckIn, CheckInProtectedReport } from "./checkin";
import * as _m0 from "protobufjs/minimal";

export const protobufPackage = "";

export interface SubmissionPayload {
  keys?: TemporaryExposureKey[];
  requestPadding?: Uint8Array;
  visitedCountries?: string[];
  origin?: string;
  consentToFederation?: boolean;
  /** @deprecated */
  checkIns?: CheckIn[];
  submissionType?: SubmissionPayload_SubmissionType;
  checkInProtectedReports?: CheckInProtectedReport[];
}

export enum SubmissionPayload_SubmissionType {
  SUBMISSION_TYPE_PCR_TEST = 0,
  SUBMISSION_TYPE_RAPID_TEST = 1,
  SUBMISSION_TYPE_HOST_WARNING = 2,
  UNRECOGNIZED = -1,
}

export function submissionPayload_SubmissionTypeFromJSON(
  object: any
): SubmissionPayload_SubmissionType {
  switch (object) {
    case 0:
    case "SUBMISSION_TYPE_PCR_TEST":
      return SubmissionPayload_SubmissionType.SUBMISSION_TYPE_PCR_TEST;
    case 1:
    case "SUBMISSION_TYPE_RAPID_TEST":
      return SubmissionPayload_SubmissionType.SUBMISSION_TYPE_RAPID_TEST;
    case 2:
    case "SUBMISSION_TYPE_HOST_WARNING":
      return SubmissionPayload_SubmissionType.SUBMISSION_TYPE_HOST_WARNING;
    case -1:
    case "UNRECOGNIZED":
    default:
      return SubmissionPayload_SubmissionType.UNRECOGNIZED;
  }
}

export function submissionPayload_SubmissionTypeToJSON(
  object: SubmissionPayload_SubmissionType
): string {
  switch (object) {
    case SubmissionPayload_SubmissionType.SUBMISSION_TYPE_PCR_TEST:
      return "SUBMISSION_TYPE_PCR_TEST";
    case SubmissionPayload_SubmissionType.SUBMISSION_TYPE_RAPID_TEST:
      return "SUBMISSION_TYPE_RAPID_TEST";
    case SubmissionPayload_SubmissionType.SUBMISSION_TYPE_HOST_WARNING:
      return "SUBMISSION_TYPE_HOST_WARNING";
    case SubmissionPayload_SubmissionType.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

function createBaseSubmissionPayload(): SubmissionPayload {
  return {
    keys: [],
    requestPadding: new Uint8Array(),
    visitedCountries: [],
    origin: "",
    consentToFederation: false,
    checkIns: [],
    submissionType: 0,
    checkInProtectedReports: [],
  };
}

export const SubmissionPayload = {
  encode(
    message: SubmissionPayload,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.keys !== undefined && message.keys.length !== 0) {
      for (const v of message.keys) {
        TemporaryExposureKey.encode(v!, writer.uint32(10).fork()).ldelim();
      }
    }
    if (
      message.requestPadding !== undefined &&
      message.requestPadding.length !== 0
    ) {
      writer.uint32(18).bytes(message.requestPadding);
    }
    if (
      message.visitedCountries !== undefined &&
      message.visitedCountries.length !== 0
    ) {
      for (const v of message.visitedCountries) {
        writer.uint32(26).string(v!);
      }
    }
    if (message.origin !== undefined && message.origin !== "") {
      writer.uint32(34).string(message.origin);
    }
    if (message.consentToFederation === true) {
      writer.uint32(40).bool(message.consentToFederation);
    }
    if (message.checkIns !== undefined && message.checkIns.length !== 0) {
      for (const v of message.checkIns) {
        CheckIn.encode(v!, writer.uint32(50).fork()).ldelim();
      }
    }
    if (message.submissionType !== undefined && message.submissionType !== 0) {
      writer.uint32(56).int32(message.submissionType);
    }
    if (
      message.checkInProtectedReports !== undefined &&
      message.checkInProtectedReports.length !== 0
    ) {
      for (const v of message.checkInProtectedReports) {
        CheckInProtectedReport.encode(v!, writer.uint32(66).fork()).ldelim();
      }
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SubmissionPayload {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseSubmissionPayload();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.keys!.push(
            TemporaryExposureKey.decode(reader, reader.uint32())
          );
          break;
        case 2:
          message.requestPadding = reader.bytes();
          break;
        case 3:
          message.visitedCountries!.push(reader.string());
          break;
        case 4:
          message.origin = reader.string();
          break;
        case 5:
          message.consentToFederation = reader.bool();
          break;
        case 6:
          message.checkIns!.push(CheckIn.decode(reader, reader.uint32()));
          break;
        case 7:
          message.submissionType = reader.int32() as any;
          break;
        case 8:
          message.checkInProtectedReports!.push(
            CheckInProtectedReport.decode(reader, reader.uint32())
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): SubmissionPayload {
    return {
      keys: Array.isArray(object?.keys)
        ? object.keys.map((e: any) => TemporaryExposureKey.fromJSON(e))
        : [],
      requestPadding: isSet(object.requestPadding)
        ? bytesFromBase64(object.requestPadding)
        : new Uint8Array(),
      visitedCountries: Array.isArray(object?.visitedCountries)
        ? object.visitedCountries.map((e: any) => String(e))
        : [],
      origin: isSet(object.origin) ? String(object.origin) : "",
      consentToFederation: isSet(object.consentToFederation)
        ? Boolean(object.consentToFederation)
        : false,
      checkIns: Array.isArray(object?.checkIns)
        ? object.checkIns.map((e: any) => CheckIn.fromJSON(e))
        : [],
      submissionType: isSet(object.submissionType)
        ? submissionPayload_SubmissionTypeFromJSON(object.submissionType)
        : 0,
      checkInProtectedReports: Array.isArray(object?.checkInProtectedReports)
        ? object.checkInProtectedReports.map((e: any) =>
            CheckInProtectedReport.fromJSON(e)
          )
        : [],
    };
  },

  toJSON(message: SubmissionPayload): unknown {
    const obj: any = {};
    if (message.keys) {
      obj.keys = message.keys.map((e) =>
        e ? TemporaryExposureKey.toJSON(e) : undefined
      );
    } else {
      obj.keys = [];
    }
    message.requestPadding !== undefined &&
      (obj.requestPadding = base64FromBytes(
        message.requestPadding !== undefined
          ? message.requestPadding
          : new Uint8Array()
      ));
    if (message.visitedCountries) {
      obj.visitedCountries = message.visitedCountries.map((e) => e);
    } else {
      obj.visitedCountries = [];
    }
    message.origin !== undefined && (obj.origin = message.origin);
    message.consentToFederation !== undefined &&
      (obj.consentToFederation = message.consentToFederation);
    if (message.checkIns) {
      obj.checkIns = message.checkIns.map((e) =>
        e ? CheckIn.toJSON(e) : undefined
      );
    } else {
      obj.checkIns = [];
    }
    message.submissionType !== undefined &&
      (obj.submissionType = submissionPayload_SubmissionTypeToJSON(
        message.submissionType
      ));
    if (message.checkInProtectedReports) {
      obj.checkInProtectedReports = message.checkInProtectedReports.map((e) =>
        e ? CheckInProtectedReport.toJSON(e) : undefined
      );
    } else {
      obj.checkInProtectedReports = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<SubmissionPayload>, I>>(
    object: I
  ): SubmissionPayload {
    const message = createBaseSubmissionPayload();
    message.keys =
      object.keys?.map((e) => TemporaryExposureKey.fromPartial(e)) || [];
    message.requestPadding = object.requestPadding ?? new Uint8Array();
    message.visitedCountries = object.visitedCountries?.map((e) => e) || [];
    message.origin = object.origin ?? "";
    message.consentToFederation = object.consentToFederation ?? false;
    message.checkIns =
      object.checkIns?.map((e) => CheckIn.fromPartial(e)) || [];
    message.submissionType = object.submissionType ?? 0;
    message.checkInProtectedReports =
      object.checkInProtectedReports?.map((e) =>
        CheckInProtectedReport.fromPartial(e)
      ) || [];
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

const atob: (b64: string) => string =
  globalThis.atob ||
  ((b64) => globalThis.Buffer.from(b64, "base64").toString("binary"));
function bytesFromBase64(b64: string): Uint8Array {
  const bin = atob(b64);
  const arr = new Uint8Array(bin.length);
  for (let i = 0; i < bin.length; ++i) {
    arr[i] = bin.charCodeAt(i);
  }
  return arr;
}

const btoa: (bin: string) => string =
  globalThis.btoa ||
  ((bin) => globalThis.Buffer.from(bin, "binary").toString("base64"));
function base64FromBytes(arr: Uint8Array): string {
  const bin: string[] = [];
  arr.forEach((byte) => {
    bin.push(String.fromCharCode(byte));
  });
  return btoa(bin.join(""));
}

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;

export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin
  ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & Record<
        Exclude<keyof I, KeysOfUnion<P>>,
        never
      >;

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
