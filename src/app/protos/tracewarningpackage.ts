/* eslint-disable */
import { CheckInProtectedReport } from "./checkin";
import * as _m0 from "protobufjs/minimal";

export const protobufPackage = "";

export interface TraceWarningPackage {
  /** hours since UNIX Epoch */
  intervalNumber?: number;
  region?: string;
  /** @deprecated */
  timeIntervalWarnings?: TraceTimeIntervalWarning[];
  checkInProtectedReports?: CheckInProtectedReport[];
}

export interface TraceTimeIntervalWarning {
  /** SHA-256 of the Location ID */
  locationIdHash?: Uint8Array;
  /** 10-minute intervals since UNIX Epoch */
  startIntervalNumber?: number;
  /** Number of 10-minute intervals to which the warning applies */
  period?: number;
  transmissionRiskLevel?: number;
}

function createBaseTraceWarningPackage(): TraceWarningPackage {
  return {
    intervalNumber: 0,
    region: "",
    timeIntervalWarnings: [],
    checkInProtectedReports: [],
  };
}

export const TraceWarningPackage = {
  encode(
    message: TraceWarningPackage,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.intervalNumber !== undefined && message.intervalNumber !== 0) {
      writer.uint32(8).uint32(message.intervalNumber);
    }
    if (message.region !== undefined && message.region !== "") {
      writer.uint32(18).string(message.region);
    }
    if (
      message.timeIntervalWarnings !== undefined &&
      message.timeIntervalWarnings.length !== 0
    ) {
      for (const v of message.timeIntervalWarnings) {
        TraceTimeIntervalWarning.encode(v!, writer.uint32(26).fork()).ldelim();
      }
    }
    if (
      message.checkInProtectedReports !== undefined &&
      message.checkInProtectedReports.length !== 0
    ) {
      for (const v of message.checkInProtectedReports) {
        CheckInProtectedReport.encode(v!, writer.uint32(34).fork()).ldelim();
      }
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): TraceWarningPackage {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseTraceWarningPackage();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.intervalNumber = reader.uint32();
          break;
        case 2:
          message.region = reader.string();
          break;
        case 3:
          message.timeIntervalWarnings!.push(
            TraceTimeIntervalWarning.decode(reader, reader.uint32())
          );
          break;
        case 4:
          message.checkInProtectedReports!.push(
            CheckInProtectedReport.decode(reader, reader.uint32())
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): TraceWarningPackage {
    return {
      intervalNumber: isSet(object.intervalNumber)
        ? Number(object.intervalNumber)
        : 0,
      region: isSet(object.region) ? String(object.region) : "",
      timeIntervalWarnings: Array.isArray(object?.timeIntervalWarnings)
        ? object.timeIntervalWarnings.map((e: any) =>
            TraceTimeIntervalWarning.fromJSON(e)
          )
        : [],
      checkInProtectedReports: Array.isArray(object?.checkInProtectedReports)
        ? object.checkInProtectedReports.map((e: any) =>
            CheckInProtectedReport.fromJSON(e)
          )
        : [],
    };
  },

  toJSON(message: TraceWarningPackage): unknown {
    const obj: any = {};
    message.intervalNumber !== undefined &&
      (obj.intervalNumber = Math.round(message.intervalNumber));
    message.region !== undefined && (obj.region = message.region);
    if (message.timeIntervalWarnings) {
      obj.timeIntervalWarnings = message.timeIntervalWarnings.map((e) =>
        e ? TraceTimeIntervalWarning.toJSON(e) : undefined
      );
    } else {
      obj.timeIntervalWarnings = [];
    }
    if (message.checkInProtectedReports) {
      obj.checkInProtectedReports = message.checkInProtectedReports.map((e) =>
        e ? CheckInProtectedReport.toJSON(e) : undefined
      );
    } else {
      obj.checkInProtectedReports = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<TraceWarningPackage>, I>>(
    object: I
  ): TraceWarningPackage {
    const message = createBaseTraceWarningPackage();
    message.intervalNumber = object.intervalNumber ?? 0;
    message.region = object.region ?? "";
    message.timeIntervalWarnings =
      object.timeIntervalWarnings?.map((e) =>
        TraceTimeIntervalWarning.fromPartial(e)
      ) || [];
    message.checkInProtectedReports =
      object.checkInProtectedReports?.map((e) =>
        CheckInProtectedReport.fromPartial(e)
      ) || [];
    return message;
  },
};

function createBaseTraceTimeIntervalWarning(): TraceTimeIntervalWarning {
  return {
    locationIdHash: new Uint8Array(),
    startIntervalNumber: 0,
    period: 0,
    transmissionRiskLevel: 0,
  };
}

export const TraceTimeIntervalWarning = {
  encode(
    message: TraceTimeIntervalWarning,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (
      message.locationIdHash !== undefined &&
      message.locationIdHash.length !== 0
    ) {
      writer.uint32(10).bytes(message.locationIdHash);
    }
    if (
      message.startIntervalNumber !== undefined &&
      message.startIntervalNumber !== 0
    ) {
      writer.uint32(16).uint32(message.startIntervalNumber);
    }
    if (message.period !== undefined && message.period !== 0) {
      writer.uint32(24).uint32(message.period);
    }
    if (
      message.transmissionRiskLevel !== undefined &&
      message.transmissionRiskLevel !== 0
    ) {
      writer.uint32(32).uint32(message.transmissionRiskLevel);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): TraceTimeIntervalWarning {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseTraceTimeIntervalWarning();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.locationIdHash = reader.bytes();
          break;
        case 2:
          message.startIntervalNumber = reader.uint32();
          break;
        case 3:
          message.period = reader.uint32();
          break;
        case 4:
          message.transmissionRiskLevel = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): TraceTimeIntervalWarning {
    return {
      locationIdHash: isSet(object.locationIdHash)
        ? bytesFromBase64(object.locationIdHash)
        : new Uint8Array(),
      startIntervalNumber: isSet(object.startIntervalNumber)
        ? Number(object.startIntervalNumber)
        : 0,
      period: isSet(object.period) ? Number(object.period) : 0,
      transmissionRiskLevel: isSet(object.transmissionRiskLevel)
        ? Number(object.transmissionRiskLevel)
        : 0,
    };
  },

  toJSON(message: TraceTimeIntervalWarning): unknown {
    const obj: any = {};
    message.locationIdHash !== undefined &&
      (obj.locationIdHash = base64FromBytes(
        message.locationIdHash !== undefined
          ? message.locationIdHash
          : new Uint8Array()
      ));
    message.startIntervalNumber !== undefined &&
      (obj.startIntervalNumber = Math.round(message.startIntervalNumber));
    message.period !== undefined && (obj.period = Math.round(message.period));
    message.transmissionRiskLevel !== undefined &&
      (obj.transmissionRiskLevel = Math.round(message.transmissionRiskLevel));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<TraceTimeIntervalWarning>, I>>(
    object: I
  ): TraceTimeIntervalWarning {
    const message = createBaseTraceTimeIntervalWarning();
    message.locationIdHash = object.locationIdHash ?? new Uint8Array();
    message.startIntervalNumber = object.startIntervalNumber ?? 0;
    message.period = object.period ?? 0;
    message.transmissionRiskLevel = object.transmissionRiskLevel ?? 0;
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

const atob: (b64: string) => string =
  globalThis.atob ||
  ((b64) => globalThis.Buffer.from(b64, "base64").toString("binary"));
function bytesFromBase64(b64: string): Uint8Array {
  const bin = atob(b64);
  const arr = new Uint8Array(bin.length);
  for (let i = 0; i < bin.length; ++i) {
    arr[i] = bin.charCodeAt(i);
  }
  return arr;
}

const btoa: (bin: string) => string =
  globalThis.btoa ||
  ((bin) => globalThis.Buffer.from(bin, "binary").toString("base64"));
function base64FromBytes(arr: Uint8Array): string {
  const bin: string[] = [];
  arr.forEach((byte) => {
    bin.push(String.fromCharCode(byte));
  });
  return btoa(bin.join(""));
}

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;

export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin
  ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & Record<
        Exclude<keyof I, KeysOfUnion<P>>,
        never
      >;

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
