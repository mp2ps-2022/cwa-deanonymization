import { Injectable } from '@angular/core';
import { CheckIn } from '../protos/checkin';
import { Utils } from '../utils/utils';

@Injectable({
  providedIn: 'root'
})
export class CheckInService {

  qrCodes: string[] = [];
  locations: {
    index: number,
    locationId: string,
    description: string,
    address: string,
    eventStart?: number,
    eventEnd?: number
  }[] = [];
  checkIns: CheckIn[] = [];

  constructor() { }

  async checkIn(index: number) {
    const url = this.qrCodes[index];
    const data = Utils.readQrCodeData(url);

    if (data === null)
      return;

    // calculate random stay
    // const qrCode = data.message.toJSON();
    // let eventStart = Number(qrCode['locationData']['startTimestamp']);
    // let eventEnd = Number(qrCode['locationData']['endTimestamp']);
    let eventStart = data.message.locationData?.startTimestamp;
    let eventEnd = data.message.locationData?.endTimestamp;

    const MIN_STAY = 60 * 60;
    let stay: number = MIN_STAY;
    let offset: number = 0;

    if (eventStart && eventEnd) {
      // Zufälligen Aufenthalt im Veranstaltungszeitraum berechnen
      offset = Math.round(Math.random() * ((eventEnd - MIN_STAY) - eventStart));
      stay = Math.round(Math.random() * (eventEnd - (eventStart + offset)));
    } else {
      let a = Math.floor(Date.now() / 1000);
      let b = Math.floor(Math.random() * 24);
      eventStart = a - (b * 60 * 60);
    }

    let checkInPayload: CheckIn = {
      locationId: data.locationId,
      startIntervalNumber: eventStart + offset,
      endIntervalNumber: eventStart + offset + stay,
      transmissionRiskLevel: Math.ceil(Math.random() * 8)
    };

    this.checkIns.push(checkInPayload);
  }
}
