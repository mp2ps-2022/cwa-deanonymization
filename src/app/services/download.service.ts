import { Injectable } from '@angular/core';
import * as zip from '@zip.js/zip.js';
import * as S3 from 'aws-sdk/clients/s3';
import { Buffer } from 'buffer';
import sha256 from 'fast-sha256';
import aes from 'js-crypto-aes';
import { CheckInProtectedReport, CheckInRecord } from '../protos/checkin';
import { TraceWarningPackage } from '../protos/tracewarningpackage';
import { CheckInService } from './check-in.service';

@Injectable({
  providedIn: 'root'
})
export class DownloadService {

  infections: {
    [locationId: string]: {
      start: number,
      end: number
    }[]
  } = {}

  constructor(
    private checkInService: CheckInService
  ) { }

  loadInfections() {
    const infections = localStorage.getItem('infections');

    if (infections) {
      this.infections = JSON.parse(infections);
    }
  }

  async checkInfections() {
    const reports = await this.getInfectedLocationReports();

    console.log(`Found ${reports.length} reports:`, reports);

    for (let location of this.checkInService.locations) {
      const locationId = Buffer.from(location.locationId, 'base64');
      const locationIdDoubleHashedB64 = Buffer.from(sha256(locationId)).toString('base64');

      // console.log(`Checking location ${location.locationId}`)

      let infectionsFound = 0;

      for (let report of reports) {
        if (!report.locationIdHash) continue;

        const reportLocationIdB64 = Buffer.from(report.locationIdHash).toString('base64');

        if (locationIdDoubleHashedB64 === reportLocationIdB64) {
          infectionsFound++;

          const decryptedRecord = await this.decryptCheckInRecord(report.encryptedCheckInRecord!, report.iv!, locationId);

          const start = decryptedRecord.startIntervalNumber! * 10 * 60;
          const end = start + decryptedRecord.period! * 10 * 60;

          if (!this.infections[location.locationId]) {
            this.infections[location.locationId] = [];
          }

          let infection = this.infections[location.locationId].find(i => i.start === start && i.end === end);

          if (!infection) {
            this.infections[location.locationId].push({
              start: start,
              end: end
            });

            console.log('--- INFECTION ---')
            console.log('Adress', location.address)
            console.log('Description', location.description)
            console.log('Start', new Date(start * 1000))
            console.log('End', new Date(end * 1000))
            console.log('-----------------')
          } else {
            // console.log('Infection already tracked. Skipping this one.')
          }
        }
      }

      // console.log(`Found ${infectionsFound} infections at location ${location.locationId}`)
    }

    localStorage.setItem('infections', JSON.stringify(this.infections));
  }

  private async decryptCheckInRecord(record: Uint8Array, iv: Uint8Array, location_id: Uint8Array) {
    const key = sha256(Buffer.concat([Buffer.from('4357412d454e4352595054494f4e2d4b4559', 'hex'), location_id]));
    const decrypted = await aes.decrypt(record, key, { name: 'AES-CBC', iv: iv });

    return CheckInRecord.decode(decrypted)
  }

  private async getInfectedLocationReports() {
    let reports: CheckInProtectedReport[] = [];

    for (let version of await this.versions()) {
      for (let country of await this.countries(version)) {
        for (let hour of await this.hours(version, country)) {
          if (hour === null)
            continue;

          const hourData = await this.hourData(version, country, hour);

          if (hourData) {
            const dataReader = new zip.Data64URIReader(hourData)
            const zipReader = new zip.ZipReader(dataReader);
            const entries = await zipReader.getEntries();

            const exportBin = entries.find(e => e.filename === 'export.bin');

            if (exportBin) {
              const exportBinData = await exportBin.getData?.(new zip.Uint8ArrayWriter());

              if (exportBinData) {
                const message = TraceWarningPackage.decode(exportBinData);
                const checkInProtectedReports = message.checkInProtectedReports

                if (checkInProtectedReports) {
                  reports = reports.concat(checkInProtectedReports)
                }
              }
            } else {
              console.error(`Bad export ${version}/${country}/${hour}`);
            }
          }
        }
      }
    }

    return reports;
  }

  private getS3Client() {
    return new S3({
      credentials: {
        accessKeyId: 'accessKey1',
        secretAccessKey: 'verySecretKey1'
      },
      endpoint: 'http://localhost:8003'
    });
  }

  private async versions(): Promise<string[]> {
    const res = await this.getJSON('version/index-v2');

    return res && Array.isArray(res) ? res : [];
  }

  private async countries(version: string): Promise<string[]> {
    const res = await this.getJSON(`version/${version}/twp/country`);

    return res && Array.isArray(res) ? res : [];
  }

  private async hours(version: string, country: string): Promise<string[]> {
    const res = await this.getJSON(`version/${version}/twp/country/${country}/hour`);

    return res ? Object.values(res) : [];
  }

  private async hourData(version: string, country: string, hour: string) {
    const res = await this.get(`version/${version}/twp/country/${country}/hour/${hour}`);

    return res ? res.toString('base64') : null;
  }

  private async getJSON(key: string) {
    const res = await this.get(key);

    console.log(`Got JSON for key ${key}:`, res?.toString());

    return res ? JSON.parse(res.toString()) : null;
  }

  private async get(key: string): Promise<S3.Body | undefined> {
    try {
      const res = await this.getS3Client().getObject({ Bucket: 'cwa', Key: key }).promise()

      return res.Body;
    } catch (err) {
      console.error('Error while fetching key ' + key + ':', err);
    }

    return;
  }
}
