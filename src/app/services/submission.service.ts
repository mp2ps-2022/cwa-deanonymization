import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import axios from 'axios';
import { Buffer } from 'buffer';
import { randomBytes } from 'crypto';
import sha256, { hmac } from 'fast-sha256';
import aes from 'js-crypto-aes';
import { CheckIn, CheckInProtectedReport, CheckInRecord } from '../protos/checkin';
import { TemporaryExposureKey, TemporaryExposureKey_ReportType } from '../protos/infections';
import { SubmissionPayload } from '../protos/submission';
import { CheckInService } from './check-in.service';

@Injectable({
  providedIn: 'root'
})
export class SubmissionService {

  private readonly NUMBER_OF_KEYS: number = 10;
  private readonly NUMBER_OF_CHECKINS: number = 1; // 2
  private readonly TRANSMISSION_RISK_LEVEL: number = 6;
  private readonly ROLLING_PERIOD: number = 144; // 24*60/10
  private readonly REPORT_TYPE: number = TemporaryExposureKey_ReportType.CONFIRMED_TEST; // 1
  private readonly VISITED_COUNTRIES: string[] = ['DE'];
  private readonly ORIGIN_COUNTRY: string = 'DE';
  private readonly CONSENT_TO_FEDERATION: boolean = true;
  private readonly DAYS_SINCE_ONSET_OF_SYMPTOMS: number = 0;

  constructor(
    private httpClient: HttpClient,
    private checkInService: CheckInService
  ) { }

  async createSubmission(index?: number) {
    // Create Submission Payload
    let now = new Date();
    let todayMidnight = new Date(now.getFullYear(), now.getMonth(), now.getDate());
    let todayMidnightMinusNumKeys = new Date(new Date(todayMidnight).setDate(todayMidnight.getDate() - this.NUMBER_OF_KEYS));
    let todayMidnightMinusNumCheckIns = new Date(new Date(todayMidnight).setDate(todayMidnight.getDate() - this.NUMBER_OF_CHECKINS));

    let checkIns: CheckInProtectedReport[];

    if (index !== undefined) {
      const locationId = Buffer.from(this.checkInService.locations[index].locationId, 'base64');
      checkIns = await this.generateRandomProtectedCheckInReportsForLocation(this.NUMBER_OF_CHECKINS, todayMidnightMinusNumCheckIns, locationId);
    } else {
      if (this.checkInService.checkIns.length === 0) {
        return;
      }

      checkIns = await this.generateProtectedCheckInReportsForCheckIns(this.checkInService.checkIns);
    }

    console.log(todayMidnight, todayMidnightMinusNumKeys, todayMidnightMinusNumCheckIns);

    let submissionPayload: SubmissionPayload = {
      origin: this.ORIGIN_COUNTRY,
      visitedCountries: this.VISITED_COUNTRIES,
      consentToFederation: this.CONSENT_TO_FEDERATION,
      requestPadding: Buffer.alloc(100),
      // checkIns: this.generateCheckInsForLocation(this.NUMBER_OF_CHECKINS, todayMidnightMinusNumCheckIns, randomCheckInPayload.locationId, this.TRANSMISSION_RISK_LEVEL),
      checkInProtectedReports: checkIns,
      keys: this.generateExposureKeys(this.NUMBER_OF_KEYS, todayMidnightMinusNumKeys, this.TRANSMISSION_RISK_LEVEL, this.ROLLING_PERIOD, this.REPORT_TYPE, this.DAYS_SINCE_ONSET_OF_SYMPTOMS),
    };

    console.log('Submission request:', submissionPayload)

    let submissionPayloadMessage = SubmissionPayload.encode(submissionPayload);
    let submissionPayloadBuffer = submissionPayloadMessage.finish();

    axios.post('https://localhost:8000/version/v1/diagnosis-keys', Buffer.from(submissionPayloadBuffer), {
      headers: {
        'CWA-Authorization': 'edc07f08-a1aa-11ea-bb37-0242ac130002',
        'CWA-Fake': '0',
        'Content-Type': 'application/x-protobuf',
      }
    }).then((res) => {
      console.log(res);
    }, (rej) => {
      console.log(rej);
    });
  }

  private generateExposureKeys(num: number, today: Date, risk: number, rollingPeriod: number, reportType: number, daysSinceOnsetOfSymptoms: number) {
    let result: TemporaryExposureKey[] = [];

    let time = Math.floor((today.getTime() / 1000 / 600));

    for (let i = 0; i < num; i++) {
      let data = randomBytes(16);

      const payload: TemporaryExposureKey = {
        keyData: data,
        transmissionRiskLevel: risk,
        rollingStartIntervalNumber: time + (rollingPeriod * i),
        rollingPeriod: rollingPeriod,
        reportType: reportType,
        daysSinceOnsetOfSymptoms: daysSinceOnsetOfSymptoms
      };

      result.push(payload);
    }

    return result;
  }

  // private generateCheckInsForLocation(num: number, today: Date, locationId: Uint8Array, risk: number) {
  //   const result: CheckIn[] = [];

  //   for (let i = 0; i < num; i++) {
  //     const start = new Date(new Date(new Date(today.setDate(today.getDate() + i)).setHours(Math.floor(Math.random() * 24))).setMinutes(Math.floor(Math.random() * 50)));
  //     const end = new Date(new Date(start).setMinutes(start.getMinutes() + 5 + Math.floor(Math.random() * 45))) // 5 - 50 Min Aufenthalt

  //     const payload: CheckIn = {
  //       locationId: locationId,
  //       startIntervalNumber: Math.floor(start.getTime() / 1000),
  //       endIntervalNumber: Math.floor(end.getTime() / 1000),
  //       transmissionRiskLevel: risk
  //     };

  //     result.push(payload)
  //   }

  //   return result
  // }

  private async generateProtectedCheckInReportsForCheckIns(checkIns: CheckIn[]) {
    let result: CheckInProtectedReport[] = [];

    for (let checkIn of checkIns) {
      let start = Math.floor(checkIn.startIntervalNumber! / (10 * 60));
      let end = Math.floor(checkIn.endIntervalNumber! / (10 * 60));
      let period = end - start;

      const locationId = checkIn.locationId!;
      const checkInRecord: CheckInRecord = {
        startIntervalNumber: start,
        period: period,
        transmissionRiskLevel: checkIn.transmissionRiskLevel
      };

      result.push(await this.generateCheckInProtectedReport(locationId, checkInRecord));
    }

    return result;
  }

  private async generateRandomProtectedCheckInReportsForLocation(num: number, today: Date, locationId: Uint8Array) {
    let result: CheckInProtectedReport[] = [];

    for (let i = 0; i < num; i++) {
      // const start: Date = new Date(new Date(today.setDate(today.getDate() + i)).setHours(Math.floor(Math.random() * 24)));
      // const duration = 60 + Math.floor(Math.random() * 60 * 50); // 1 Min bis 50 Min

      // console.log(start, duration);

      const duration = 60 * 30;
      const start = Math.floor(Date.now() / 1000);
      let end_time = start + duration;
      let start_time_agg = Math.floor(start / (10 * 60));
      let end_time_agg = Math.floor(end_time / (10 * 60));
      let period = end_time_agg - start_time_agg;

      const checkInRecord: CheckInRecord = {
        startIntervalNumber: start_time_agg, // Math.floor(start.getTime() / 1000),
        period: period,
        transmissionRiskLevel: 2
      };

      result.push(await this.generateCheckInProtectedReport(locationId, checkInRecord));
    }

    return result;
  }

  private async generateCheckInProtectedReport(locationId: Uint8Array, checkInRecord: CheckInRecord) {
    const checkInRecordBuffer = CheckInRecord.encode(checkInRecord).finish();

    // Siehe CheckInCryptography.kt
    const iv = randomBytes(16);
    const enckey = sha256(Buffer.concat([Buffer.from('4357412d454e4352595054494f4e2d4b4559', 'hex'), Buffer.from(locationId)]));

    const encryptedRecord = Buffer.from(await aes.encrypt(checkInRecordBuffer, enckey, { name: 'AES-CBC', iv: iv }));

    const mackey = sha256(Buffer.concat([Buffer.from('4357412d4d41432d4b4559', 'hex'), Buffer.from(locationId)]));
    const mac = hmac(mackey, Buffer.concat([iv, encryptedRecord]));

    const checkInProtectedReport: CheckInProtectedReport = {
      locationIdHash: sha256(locationId), // zweifach gehasht
      iv: iv,
      encryptedCheckInRecord: encryptedRecord,
      mac: mac
    };

    return checkInProtectedReport;
  }
}
