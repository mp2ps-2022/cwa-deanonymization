import { Buffer } from 'buffer';
import sha256 from 'fast-sha256';
import { QRCodePayload } from '../protos/qr';

export class Utils {

  static readQrCodeData(url: string) {
    let data = url.split('#')[1].trim();
    data += '='.repeat(data.length % 3);
    let buffer = Buffer.from(data, 'base64');

    let message = QRCodePayload.decode(buffer);

    return { message: message, locationId: Utils.getLocationId(buffer) };
  }

  static getLocationId(qrCodeBuffer: Buffer) {
    return sha256(Buffer.concat([Buffer.from('CWA-GUID', 'utf8'), qrCodeBuffer]));
  }
}
